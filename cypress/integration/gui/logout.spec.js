/// <reference types="Cypress" />

import url from '../../../cypress.json'

describe('Logout', () => {
    before(() => cy.login())

    it('successfully', () => {
        cy.logout()

        cy.url().should('be.equal', `${url['url-sb']}public/login`)
    })
})