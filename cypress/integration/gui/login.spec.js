/// <reference types="Cypress" />

import url from '../../../cypress.json'

describe('Login', () => {
    it('successfully', () => {
        cy.login()
        
        cy.wait(5000)
        cy.get('#dropdown-menu-toggle').should('be.visible')
        cy.url().should('be.equal', `${url['url-sb']}private/home`)
    })
})