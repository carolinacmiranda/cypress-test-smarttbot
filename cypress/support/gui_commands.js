/// <reference types="Cypress" />

const url = require('../../cypress.json')
const info = require('../../cypress.env.json')

Cypress.Commands.add('login', () => {
    cy.visit(url['url-sb'])

    cy.get('#login-username').type(info.user_name)
    cy.get('#login-password').type(info.user_password)
    cy.get('#login-button').click()
})

Cypress.Commands.add('logout', () => {
    cy.wait(5000)
    cy.get('#dropdown-menu-toggle').click()
    cy.get('#user-actions-list-logout').click()
})